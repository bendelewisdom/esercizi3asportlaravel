<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dati ottenuti dalla chiamata API</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <h1>Dati ottenuti dalla chiamata API</h1>

        @if (isset($data['stories']) && !empty($data['stories']))
            <h2>Storie dei Brand</h2>
            <ul class="list-group">
                @foreach ($data['stories'] as $story)
                    <li class="list-group-item">
                        <h4>ID: {{ $story['id'] }}</h4>
                        <p><strong>Nome:</strong> {{ $story['nome'] }}</p>
                        <p><strong>Storia:</strong> {{ $story['storia'] }}</p>
                    </li>
                @endforeach
            </ul>
        @else
            <p>Nessuna storia disponibile al momento.</p>
        @endif

        @if (isset($data['locations']) && !empty($data['locations']))
            <h2>Posizioni dei Brand</h2>
            <ul class="list-group">
                @foreach ($data['locations'] as $location)
                    <li class="list-group-item">
                        <h4>ID: {{ $location['id'] }}</h4>
                        <p><strong>Nome:</strong> {{ $location['nome'] }}</p>
                        <p><strong>Città:</strong> {{ $location['città'] }}</p>
                        <p><strong>Stato:</strong> {{ $location['stato'] }}</p>
                        <p><strong>Latitudine:</strong> {{ $location['latitudine'] }}</p>
                        <p><strong>Longitudine:</strong> {{ $location['longitudine'] }}</p>
                    </li>
                @endforeach
            </ul>
        @else
            <p>Nessuna posizione disponibile al momento.</p>
        @endif
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
