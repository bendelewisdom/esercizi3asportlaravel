<!-- resources/views/news.blade.php -->

<!DOCTYPE html>
<html>
<head>
    <title>Arbeit Now Job Board</title>
</head>
<body>
    <h1>Arbeit Now Job Board</h1>

    @if (isset($error))
        <p>{{ $error }}</p>
    @elseif (isset($jobBoardData))
        @if (count($jobBoardData) > 0)
            <ul>
                @foreach ($jobBoardData as $job)
                    <li>
                        <h3>{{ $job['title'] }}</h3>
                        <p>{{ $job['description'] }}</p>
                        <p><strong>Location:</strong> {{ $job['location'] }}</p>
                        <!-- Aggiungi altre informazioni del lavoro qui -->
                    </li>
                @endforeach
            </ul>
        @else
            <p>Nessun lavoro disponibile al momento.</p>
        @endif
    @else
        <p>Nessun dato disponibile al momento.</p>
    @endif
</body>
</html>
