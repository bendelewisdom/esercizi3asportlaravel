<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dettagli Prodotto Shopify</title>
    <!-- Aggiungi i link ai file CSS di Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body class="d-flex justify-content-center align-items-center" style="height: 100vh;">
    <div class="card text-center" style="width: 18rem;">
        <img class="card-img-top" src="{{ $product['image']['src'] }}" alt="{{ $product['title'] }}" style="width: 286px; height: 180px;">
        <div class="card-body">
            <h5 class="card-title">{{ $product['title'] }}</h5>
            <p class="card-text">{{ $product['body_html'] }}</p>
            <p class="card-text"><strong>Fornitore:</strong> {{ $product['vendor'] }}</p>
            <p class="card-text"><strong>Price:</strong> ${{ $product['variants'][0]['price'] }}</p>         
               <a href="#" class="btn btn-primary">Go somewhere</a>
        </div>
    </div>

    <!-- Aggiungi i link ai file JavaScript di Bootstrap (opzionale, se necessario) -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
