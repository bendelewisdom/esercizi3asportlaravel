@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
{{-- {{ Breadcrumbs::render('create.role') }} --}}
@stop

@section('content')
<div class="container">
<div class="justify-content-center">
@if (count($errors) > 0)
<div class="alert alert-danger">
<strong>Opps!</strong> Something went wrong, please check below errors.<br><br>
<ul>
@foreach ($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif
<div class="card">
<div class="card-header">
<span class="float-left">
<a class="text-teal" href="{{ route('role.index') }}">
<i class="fas fa-arrow-alt-circle-left fa-2x"></i>
</a>
</span>
</div>
<div class="card-body">
{!! Form::open(array('route' => 'roles.store','method'=>'POST')) !!}
<div class="form-group">
<strong>Name:</strong>
{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Role Name']) !!}
<div class="form-group">
<strong>Permission:</strong> <br/>
<div class="container">

@csrf
@foreach ($permissions as $permission)
<input type="checkbox" name="permission[]" value="{{ $permission->name }}"> {{ $permission->name }}<br>
@endforeach



</div>    
</div>
</div>

<button type="submit" class="btn btn-primary">Submit</button>
{!! Form::close() !!}
</div>
</div>
</div>
</div>
@stop

@section('css')

@stop

@section('js')
<script> console.log('Hi!'); </script>
@stop
