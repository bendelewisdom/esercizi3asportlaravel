@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    {{-- {{ Breadcrumbs::render('roles') }} --}}
@stop

@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    {{-- @can('role-create') --}}
                        <span class="float-right">
                            <a class="text-teal" href="{{ route('role.create') }}">
                                <i class="fas fa-plus-square fa-2x"></i>
                            </a>
                        </span>
                    {{-- @endcan --}}
                </div>
                <div class="card-body table-responsive">
                    <table id="tb-roles" class="table table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>is Active</th>
                                <th width="280px">Edit</th>
                            </tr>
                        </thead>
                        <tbody>

                           <div class="list-group">
                            @foreach ($datar as $keyr => $itemr)
                                <tr>
                                    <td>{{ $keyr + 1 }}</td>
                                
                                    <td>{{ $itemr->name }}</td> 
                                    <td>                                
                                        {!! Form::model($itemr, ['route' => ['role.update', $itemr->id],'method' => 'PATCH']) !!} 
                                        {{-- {!! Form::hidden($name, $value, [$options]) !!} --}}
                                        {!! Form::hidden('action', 'mod-2') !!}
                                        @if ($itemr->is_active===0)
                                        {{ Form::hidden('is_active', '1') }}
                                        <button type="submit" class="btn btn-xs btn-default mx-1 text-danger"><i class="fas fa-lg fa-ban"></i></button>
                                        @else
                                        {{ Form::hidden('is_active', '0') }}
                                        <button type="submit" class="btn btn-xs btn-default mx-1 text-teal"><i class="fas fa-lg fa-check-circle"></i></button>
                                        @endif
                                    {!! Form::close() !!}
                                </td> 

                                    <td>
                                        <a class="btn btn-xs btn-default mx-1 text-primary" href="{{ route('role.edit', $itemr->id) }}">
                                        <i class="fa fa-lg fa-fw fa-pen"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- {{ $data->render() }} --}}
                </div>
            </div>
        </div>
    </div>

@stop

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" /> 
@stop


@section('js')
    <script> console.log('Hi!'); </script>
    <script type = "text/javascript" src = "//cdn.datatables.net/2.0.3/js/dataTables.min.js"></script>

    
    <script> 
            $(document).ready( function () {
            $('#tb-roles').DataTable({
            'order': [[0, 'asc']]
            });
        });
    </script>
    
    
@stop
