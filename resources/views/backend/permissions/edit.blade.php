@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    {{-- {{ Breadcrumbs::render('edit.permission', $permission) }} --}}
@stop

@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Something went wrong, please check below errors.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <span class="float-left">
                        <a class="text-teal" href="{{ route('permissions.index') }}">
                            <i class="fas fa-arrow-alt-circle-left fa-2x"></i>
                        </a>
                    </span>
                </div>
                <div class="card-body">
                    {!! Form::model($permissions, ['route' => ['permissions.update', $permissions->id], 'method'=>'PATCH']) !!}
                        <div class="form-group">
                            <strong>Name:</strong>
                            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    {!! Form::close() !!}
                   
                </div>
            </div>

            <div class="card mt-5" style="border:solid; border-color: darkred;">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-sm-12 col-md-6">
                            <div class="p-3">
                                <h2>Attenzione</h2>
                                <p>Cliccando sul cestino <span class="text-bold text-danger">eliminerai definitivamente</span> il permesso.</p>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="p-3 text-center">
                                {{-- @can('permission-delete') --}}
                                    {!! Form::model($permissions, ['route' => ['permissions.destroy', $permissions->id],'method' => 'DELETE']) !!}
                                        <button type="submit" class="btn btn-xs btn-default mx-1 text-danger"><i class="fa fa-3x fa-fw fa-trash"></i></button>
                                    {!! Form::close() !!}
                                {{-- @endcan --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@stop

@section('css')

@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
