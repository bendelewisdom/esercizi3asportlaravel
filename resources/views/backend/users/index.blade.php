@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
{{-- {{ Breadcrumbs::render('users') }} --}}
@stop

@section('content')
<div class="container">
<div class="justify-content-center">
@if (\Session::has('success'))
<div class="alert alert-success">
<p>{{ \Session::get('success') }}</p>
</div>
@endif

<div class="card">
{{-- @can('user-create') --}}
<div class="card-header">
<span class="float-right">
<a class="text-teal" href="{{ route('users.create') }}">
<i class="fas fa-plus-square fa-2x"></i>
</a>
</span>
</div>
{{-- @endcan --}}
<div class="card-body table-responsive"> 
<table id="tb-users" class="table table-hover">
<thead class="thead-dark">
<tr>
<th>Name</th>
{{-- <th>Is Active</th>  --}}
<th>Email</th>
{{-- <th>Roles</th>  --}}
<th width="280px">Edit</th>
</tr>
</thead>


<tbody>   
<div class="list-group">
@foreach ($user as $users)
<tr>
    <td>{{ $users->name }}</td>
                               
</div>
<td>{{ $users->email }}</td>
<td>

{{-- @can('usqer-edit') --}}
<a class="btn btn-xs btn-default mx-1 text-primary" href="{{ route('users.edit',$users->id) }}">
    <i class="fa fa-lg fa-fw fa-pen"></i>
</a>
{{-- @endcan --}}
</div>
@endforeach
</div>                     

</tr>
{{-- @endforeach  --}}
</tbody>
</table>
{{-- {{ $data->render() }}  --}}
</div>
</div>
</div> 
</div>  
@stop 

@section('css')
<script type = "text/javascript" src = "//cdn.datatables.net/2.0.3/css/dataTables.dataTables.min.css"></script>
@stop
@section('js')
    <script> console.log('Hi!'); </script>
    <script type = "text/javascript" src = "//cdn.datatables.net/2.0.3/js/dataTables.min.js"></script>
    
    
    <script> 
            $(document).ready( function () {
            $('#tb-users').DataTable({
            'order': [[0, 'asc']]
            });
        });
    </script>
@stop
