@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    {{-- {{ Breadcrumbs::render('edit.user', $user) }} --}}
@stop

@section('content')
    <div class="container">
         <div class="justify-content-center">
           @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Something went wrong, please check below errors.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach 
                    </ul>
                </div>
            @endif
            <div class="card-header">
                <span class="float-left">
                    <a class="text-teal" href="{{ route('user') }}">
                        <i class="fas fa-arrow-alt-circle-left fa-2x"></i>
                    </a>
                </span>
            </div> 
            {{-- @if(Auth::user()->hasRole('superadmin')) --}}
            <div class="card" style="border:solid 1px; border-color: rgba(52, 58, 64);">
                <div class="card-body">
                    {!! Form::model($user, ['route' => ['users.update', $user->id], 'method'=>'PATCH']) !!}
                    {!! Form::hidden('action', 'mod-1') !!}
                        <div class="form-group">
                            <strong>Name:</strong>
                            {!! Form::text('name', $user->name, array('placeholder' => 'Name','class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            <strong>Email:</strong>
                            {!! Form::text('email', $user->email, array('placeholder' => 'Email','class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            <strong>Role:</strong>
                            {!! Form::select('roles[]', $roles, $userRole, array('class' => 'form-control','multiple')) !!}
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    {!! Form::close() !!}
                </div>
            </div>
            {{-- @endif --}}

            {{-- @if(Auth::user()->hasRole('admin')) --}}
            <div class="card" style="border:solid 1px; border-color:rgba(52, 58, 64)">
                <div class="card-body">
                    {!! Form::model($user, ['route' => ['users.update', $user->id], 'method'=>'PATCH']) !!}
                    {!! Form::hidden('action', 'mod-2') !!}
                        <div class="form-group">
                            <strong>Password:</strong>
                            {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            <strong>Confirm Password:</strong>
                            {!! Form::password('password_confirmation', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    {!! Form::close() !!}
                </div>
            </div>
            {{-- @endif --}}
            {{-- @if(Auth::user()->hasRole('admin')) --}}
                {{-- @if (isset($datiAbilitazioni) && $datiAbilitazioni['ha_abilitazioni'] === true) --}}
                    
                    <div class="card" style="border:solid 1px; border-color:rgba(52, 58, 64)">
                        <div class="card-body">
                            {!! Form::model($user, ['route' => ['users.update', $user->id], 'method'=>'PATCH']) !!}
                            {!! Form::hidden('action', 'mod-4') !!}
                                <div class="form-group">
            {{-- @endif --}}

            {{-- @if(Auth::user()->hasRole('superadmin')) --}}
            <div class="card mt-5" style="border:solid; border-color: darkred;">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-sm-12 col-md-6">
                            <div class="p-3">
                                <h2>Attenzione</h2>
                                <p>Cliccando il cestino <span class="text-bold text-danger">eliminerai definitivamente</span> lo user.</p>
                                <p>Prima di farlo hai valutato di <span class="text-bold">disattivarlo</span>?</p>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="p-3 text-center">
                                {{-- @can('user-delete') --}}
                                {!! Form::model($user, ['route' => ['users.destroy', $user->id],'method' => 'DELETE']) !!}
                                    <button type="submit" class="btn btn-xs btn-default mx-1 text-danger"><i class="fa fa-3x fa-fw fa-trash"></i></button>
                                {!! Form::close() !!}
                                {{-- @endcan --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- @endif --}}


        </div>
    </div>
@stop

@section('css')

@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
