@extends('adminlte::page')

@section('title', 'Elenchi Marchi e Fornitori')

@section('content_header')

<div style="text-align: center;">
    <img src="{{ asset('3acustom/img/3A-logo.png') }}" alt="Logo" width="200" height="100" style="margin:0px 0px 50px 0px;">
    <p><h1>Elenchi Marchi e Fornitori</h1></p>
</div>

@stop

@section('content')

                <div class="dropdown show">
                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Elenco Marchi
                    </a>
                  
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                    
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Elenco Fornitori</h3>
                </div>
                <div class="card-body">
                    <ul class="list-group">
                        @for($i = 1; $i <= 10; $i++)
                            <li class="list-group-item">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="fornitore{{$i}}" name="fornitore{{$i}}">
                                    <label class="form-check-label" for="fornitore{{$i}}">
                                        Fornitore {{$i}}
                                    </label>
                                </div>
                            </li>
                        @endfor
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop
