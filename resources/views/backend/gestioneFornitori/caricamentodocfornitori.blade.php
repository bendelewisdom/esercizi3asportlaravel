@extends('adminlte::page')




@section('title', 'carica documenti fornitori')


@section('content_header')
    {{-- {{ Breadcrumbs::render('CaricaImmagini') }} --}}
@stop


@section('content')


<section style="padding-top:60px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success d-none" id="processaImmaginiResponse"></div>
                <div class="alert alert-warning py-1 my-3">
                    Limite massimo peso file <span class="text-bold">'50mb'. </span><br>
                    </div>
                <div class="card">
                    <div class="card-header">
                        Dropzone File Upload
                    </div>
                    <div class="card-body">
                        {{-- <form method="POST" action="{{route('dropzone.store')}}" enctype="multipart/from-data" class="dropzone dz-clickable" id="image-upload"> --}}
                           @csrf
                            <div>
                                <h3 class="text-cemter">Upload File</h3>
                                <div class="dz-default dz-message"><span>Drop files hear to upload</span></div>
                            </div>
                        </form>
                    </div>
                </div>
                <div>
                    {{-- <button class="btn btn-primary" href="{{route('processaImmaginiDropzone')}}" type="submit" role="button" id="processaImmagini">Processa Immagini</button> --}}
                    <a class="btn btn-primary" id="processaImmagini">Processa i File</a>
                </div>
            </div>
        </div>
    </div>
</section>


@stop


@section('css')


@stop


@section('js')
    <script> console.log('carica documenti fornitori');
    $("#processaImmagini").click(function(e) {
        e.preventDefault();
        // show
        // $('#loader').removeClass('d-none');
        // $('#loader').addClass('show');
        $.ajax({
            type: "POST",
            url: "#",
            data: jQuery.param({
                "_token": "{{ csrf_token() }}",
            }),
            success: function(data) {
                $('#processaImmaginiResponse').removeClass('d-none');
                $('#processaImmaginiResponse').addClass('show');
                // hide
                // $('#loader').removeClass('show');
                // $('#loader').addClass('d-none');
                var nonOkImg = data.message.non_ok;


                if (nonOkImg.length != 0) {
                    console.log("Array is not empty!")
                    // console.log(nonOkImg);
                    nonOkImg.forEach(e => {
                        $('#processaImmaginiResponse').append($("<li>").text(e + ' non ho processato questo prodotto perché non è tra i prodotti attivi'));
                    });
                }else{
                    $('#processaImmaginiResponse').html('Ho processato tutti i prodotti caricati <br>');
                }


            },
            error: function(data) {
                $('#processaImmaginiResponse').removeClass('d-none');
                $('#processaImmaginiResponse').addClass('show');
                // hide
                // $('#loader').removeClass('show');
                // $('#loader').addClass('d-none');
                console.log("errore");
                console.log(data);
                $('#processaImmaginiResponse').html('Clicca ancora su processa immagini <br>');
            },
        });
    });


    </script>


@stop


@section('plugins.Dropzone', true)
