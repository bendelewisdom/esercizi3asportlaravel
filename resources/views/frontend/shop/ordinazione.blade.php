@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content')
<div class="container">
    <div class="justify-content-center">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div>
        @endif

        <!DOCTYPE html>
        <html lang="it">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Ordina Articolo</title>
            <!-- Includi Bootstrap CSS -->
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
            <style>
                /* Aggiungi stili personalizzati qui se necessario */
                .container {
                    padding: 40px;
                }
            </style>
        </head>
        <body>

            <div style="text-align: center;">
                <img src="{{ asset('3acustom/img/3A-logo.png') }}" alt="Logo" width="200" height="100" style="margin:0px 0px 50px 0px;">
            </div>
            

            <div class="container" style="border: 5px solid #212529; border-radius: 20px;">
                <form method="POST" action="{{ route('ordinazione') }}">
                    <input type="hidden" name="codice_articolo" value="{{ $articoli->codice_articolo }}">
                    <input type="hidden" name="nome" value="{{ $articoli->nome }}">
                    <input type="hidden" name="marca" value="{{ $articoli->marca }}">
                    <input type="hidden" name="taglia" value="{{ $articoli->taglia }}">
                    <input type="hidden" name="colore" value="{{ $articoli->colore }}">
                    <input type="hidden" name="prezzo" value="{{ $articoli->prezzo }}">
                    <input type="hidden" name="created_at" value="{{ $articoli->created_at }}">


                    <h1 class="text-center">Ordina Articolo</h1>

                    @if(isset($articoli))
                        <p class="text-center">Nome Articolo: {{ $articoli->nome }}</p>
                        <p class="text-center">Descrizione: {{ $articoli->descrizione }}</p>
                        <p class="text-center">Prezzo: {{ $articoli->prezzo }}</p>
                    @else
                        <p class="text-center">Nessun articolo selezionato</p>
                    @endif

                    @csrf
                    <div class="mb-3">
                        <label for="taglia" class="form-label">Taglia:</label>
                        <select class="form-select" name="taglia" id="taglia">
                            <option value="">Seleziona una taglia</option>
                            @foreach($taglie as $id => $taglia)
                                <option value="{{ $id }}">{{ $taglia }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="mb-3">
                        <label for="colore" class="form-label">Colore:</label>
                        <select class="form-select" name="colore" id="colore">
                            <option value="">Seleziona il colore</option>
                            @foreach($colori as $id => $colore)
                                <option value="{{ $id }}">{{ $colore }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="mb-3">
                        <label for="quantita" class="form-label">Quantità:</label>
                        <input type="number" class="form-control" name="quantita" id="quantita" min="1" max="1000" value="1">
                    </div>

{{-- QUELLO GIUSTO --}}
                    <div class="mb-3">
                        <label for="colore" class="form-label">Tag YourCategoryExpenses:</label>
                        <select class="form-select" name="tag" id="tag">
                            <option value="">Seleziona il tuo CategoryExpenses</option>
                            @foreach($spese as $tag => $spesa)
                            <option value="{{ $spesa }}">{{ $spesa }}</option>
                            @endforeach
                        </select>
                    </div>

                    
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn btn-primary" style="width: 50%; background-color:#dd3333; border:0px;">Ordina</button>
                    </div>

                </form>
            </div>

            <!-- Includi Bootstrap JS -->
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
            <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" rel="stylesheet">
        </body>
        </html>
    </div>
</div>
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/2.0.3/css/dataTables.dataTables.min.css">
@stop

@section('js')
<script> console.log('Hi!'); </script>
<script type="text/javascript" src="//cdn.datatables.net/2.0.3/js/dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#tb-users').DataTable({
            'order': [
                [0, 'asc']
            ]
        });
    });
</script>
@stop
