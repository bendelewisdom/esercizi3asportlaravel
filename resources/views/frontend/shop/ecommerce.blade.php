@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
{{-- {{ Breadcrumbs::render('users') }} --}}
@stop


@section('content')
<div class="container">
    <div class="justify-content-center">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div>
        @endif

        <table id="tb-articles" class="table table-hover">
            <thead class="thead-dark">
                <tr>
                    <th>Killer Shop's</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="5"> <!-- colspan="5" per fare in modo che la cella occupi tutte le colonne -->
                        <div class="container">
                            <div class="row">
                                @foreach($prodotti as $prodotto)
                                    <div class="col-md-4 mb-4">
                                        <div class="card" style="width: 18rem;">
                                            {{-- <img class="card-img-top" src="{{ $prodotto->immagine }}" alt="{{ $prodotto->nome }}"> --}}
                                            <div class="card-body"> 
                                                <p class="card-text">Marca: {{ $prodotto->marca}}</p>
                                                <h5 class="card-title">{{ $prodotto->nome }}</h5>
                                                <p class="card-text">Descrizione: {{ $prodotto->descrizione}}</p>
                                                <p class="card-text">Prezzo: {{ $prodotto->prezzo }}</p>
                                                <!-- Altre informazioni del prodotto possono essere stampate qui -->
                                                <a href="{{ route('variants', ['id' => $prodotto->id]) }}" class="btn btn-primary">Ordina</a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach 
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        
                
            </div>
        </div>                     
    </div>
</div> 


@stop 

@section('css')
<script type = "text/javascript" src = "//cdn.datatables.net/2.0.3/css/dataTables.dataTables.min.css"></script>
@stop
@section('js')
    <script> console.log('Hi!'); </script>
    <script type = "text/javascript" src = "//cdn.datatables.net/2.0.3/js/dataTables.min.js"></script>
    
    
    <script> 
            $(document).ready( function () {
            $('#tb-articles').DataTable({
            'order': [[0, 'asc']]
            });
        });
    </script>
@stop
