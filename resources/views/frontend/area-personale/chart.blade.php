<!DOCTYPE html>
<html>
<head>
    <title>Grafico delle Categorie di Spesa</title>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
</head>
<body>
    <div style="width: 100%; height: 400px; margin: 30px;">
        <canvas id="mostraGrafico"></canvas>
    </div>

    <script>
        var ctx = document.getElementById('mostraGrafico').getContext('2d');

        // Dati passati dal controller
        var labels = {!! json_encode($labels) !!};
        var data = {!! json_encode($data) !!};

        var data = {
            labels: labels,
            datasets: [{
                data: data,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.6)',
                    'rgba(54, 162, 235, 0.6)',
                    'rgba(255, 206, 86, 0.6)',
                    'rgba(75, 192, 192, 0.6)',
                    'rgba(153, 102, 255, 0.6)'
                    // Aggiungi altri colori se necessario
                ],
                borderWidth: 1
            }]
        };

        var options = {
            responsive: true,
            maintainAspectRatio: false
        };

        var myPieChart = new Chart(ctx, {
            type: 'pie',
            data: data,
            options: options
        });
    </script>
</body>
</html>
