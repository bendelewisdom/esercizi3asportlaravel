@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
{{-- {{ Breadcrumbs::render('users') }} --}}
@stop

@section('content')
<div class="container">
    <div class="justify-content-center">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div>
        @endif

        <div class="card">
            <div class="card-body table-responsive"> 
                <table id="tb-users" class="table table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th>Codice articolo</th>
                            <th>Utente</th>
                            <th>Marca</th>
                            <th>Nome</th>
                            <th>Taglia</th>
                            <th>Colore</th>
                            <th>Prezzo</th>
                            <th>Quantità</th>
                            <th width="280px">Orario creazione Ordine</th>
                        </tr>
                    </thead>

                    <tbody>

                        <div class="list-group">
                            @foreach ($ordine as $ordina => $item )
                            <tr>
                                <td>{{ $item->codice_articolo }}</td>
                                <td>{{ $item->email }}</td>
                                <td>{{ $item->marca }}</td>
                                <td>{{ $item->nome }}</td>
                                <td>{{ $item->taglia }}</td>
                                <td>{{ $item->colore }}</td>
                                <td>{{ $item->prezzo * $item->quantita }}</td>
                                <td>{{ $item->quantita }}</td>
                                <td>{{ $item->created_at }}</td>
                                
                            </tr>
                            @endforeach
                        </table>
                        
                            </tr>
                        </table>
                        
                    </tr>
                {{-- @endforeach --}}
                        
                        
                        </tr>
                    
                    </tbody>
                </table>
            </div>
        </div>                     
    </div>
</div>


@stop 

@section('css')
<script type = "text/javascript" src = "//cdn.datatables.net/2.0.3/css/dataTables.dataTables.min.css"></script>
@stop
@section('js')
    <script> console.log('Hi!'); </script>
    <script type = "text/javascript" src = "//cdn.datatables.net/2.0.3/js/dataTables.min.js"></script>
    
    
    <script> 
            $(document).ready( function () {
            $('#tb-users').DataTable({
            'order': [[0, 'asc']]
            });
        });
    </script>
@stop
