@extends('adminlte::page')

@section('title', 'Dashboard')


@section('content_header')
    {{-- {{ Breadcrumbs::render('permissions') }} --}}
@stop


@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif


            <div class="text-center"> <!-- Div per centrare il contenuto -->
                <h1>Sezione Categorie Spesa</h1>


                @include('/frontend/area-personale/chart') <!-- Includi il grafico centrato -->
            </div>


            <div class="card">
                <div class="card-header">
                    {{-- @can('permission-create') --}}
                        <span class="float-right">
                            <a class="text-teal" href="{{ route('createcat') }}">
                                <i class="fas fa-plus-square fa-2x"></i>
                            </a>
                        </span>
                    {{-- @endcan --}}
                </div>
                <div class="card-body table-responsive">
                    <table id="tb-permissions" class="table table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th>Elenco</th>
                                <th>Le mie categorie</th>
                                <th width="280px">Modifica</th>
                            </tr>
                        </thead>
                        <tbody>


                                        <div class="list-group">
                                            @foreach ($cat as $key => $item)
                                            <tr>
                                            <td>{{ $key + 1 }}</td>
                                                    <td>
                                                   
                                                        {{ $item->tag }}
                                                    </td>
                                                   
                                                    <td>
                                                        <a class="btn btn-xs btn-default mx-1 text-primary"  href="{{ route('editcat', $item->id) }}">
                                                            <i class="fa fa-lg fa-fw fa-pen"></i>
                                                        </a> {{-- Aggiungi qui l'icona desiderata --}}
                                                    </td>
                                            @endforeach


                                   
                                        {{-- @can('permission-delete') --}}
                                            {{-- {!! Form::model($category, ['route' => ['catdestroy', $category->id],'method' => 'DELETE']) !!}
                                                <button type="submit" class="btn btn-xs btn-default mx-1 text-danger"><i class="fa fa-3x fa-fw fa-trash"></i></button>
                                            {!! Form::close() !!} --}}
                                        {{-- @endcan  --}}
                                    </td>
                                </tr>
                            {{-- @endforeach --}}
                        </tbody>
                    </table>
                    {{-- {{ $data->appends($_GET)->links() }} --}}
                </div>
            </div>

        </div>
    </div>
@stop


@section('css')
<link rel="stylesheet" href="/DataTables/datatables.css" />
@stop


@section('js')
    <script> console.log('Hi!'); </script>
    <script type = "text/javascript" src = "//cdn.datatables.net/2.0.3/js/dataTables.min.js"></script>
   
   
    <script>
            $(document).ready( function () {
            $('#tb-permissions').DataTable({
            'order': [[1, 'asc']]
            });
        });
    </script>




@stop
