@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content')
<div class="container">
    <div class="justify-content-center">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div>
        @endif
        
{{-- <style>
    .main-sidebar {
    margin-left: 0;
}
    </style> --}}
        <div class="card">
            <div class="card-body table-responsive"> 
                <table id="tb-users" class="table table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th>Data</th>
                            <th>Orari timbrature</th>
                            <th>N* giornaliero timbrature</th>
                            <th width="280px">Ore totali giornaliere</th>
                        </tr>
                    </thead>

                    <tbody>                          
                            <td>Giorno: {{ $risultato['giorno'] }}</td>
                        
                            <td>
                                @foreach($risultato['orario_timbratura'] as $orario)
                                    <li>{{ $orario }}</li>
                                @endforeach
                            </td>
                        
                            <td>Numero di Timbrature Fatte Oggi: {{ $risultato['numero_di_timbrature_fatte_oggi'] }}</td>
        
                            <td>Ore totali giornaliere: {{ $risultato['ore_totali_giornaliere'] }}</td>

            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
        </div>                     
    </div>
</div>

</body>



@stop 

@section('css')
<script type = "text/javascript" src = "//cdn.datatables.net/2.0.3/css/dataTables.dataTables.min.css"></script>
@stop
@section('js')
    <script> console.log('Hi!'); </script>
    <script type = "text/javascript" src = "//cdn.datatables.net/2.0.3/js/dataTables.min.js"></script>
    

    
    <script> 
            $(document).ready( function () {
            $('#tb-users').DataTable({
            'order': [[0, 'asc']]
            });
        });
    </script>
@stop
{{-- @section('adminlte_js')
    <script>
        $(function () {
            // Rimuovi la classe sidebar-mini se presente sul body
            $('body').removeClass('sidebar-mini');

            // Rimuovi completamente la sidebar dal DOM
            $('.main-sidebar').remove(); // Rimuovi l'elemento della sidebar

            // Modifica lo spazio del contenuto principale per occupare l'intera larghezza
            $('.content-wrapper').removeClass('sidebar-mini'); // Rimuovi la classe sidebar-mini dal contenuto
            $('.content-wrapper').css('margin-left', '0'); // Rimuovi il margine sinistro aggiunto per la sidebar
            $('.content-wrapper').css('width', '100%'); // Imposta la larghezza al 100% per riempire l'intero spazio
        });
    </script>
@stop --}}

