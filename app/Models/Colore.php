<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class Colore extends Model {

    use HasFactory;
    use HasRoles;
    

    protected $table = 'colore';

    public function articoli() {
        return $this->belongsToMany(Articolo::class);
    }
}
