<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class Taglia extends Model {

    use HasFactory;
    use HasRoles;
    
    protected $table = 'taglia';

    public function categoria() {
        return $this->belongsTo(Categoria::class);
    }
}