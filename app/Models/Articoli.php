<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class Articoli extends Model {
    
    use HasRoles;

    protected $table = 'articoli'; // Assicurati che corrisponda al nome effettivo della tabella nel database
    
    public function categoria() {
        return $this->belongsTo(Categoria::class);
    }

    public function colori() {
        return $this->belongsToMany(Colore::class);
    }
}
