<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;


class ExpenseCategory extends Model
{
    use HasFactory;
    use HasRoles;

    protected $table = 'expense_categories';
    protected $fillable = ['tag', 'user_id'];


    public function user()
    {
        return $this->belongsTo(User::class, 'email', 'email');
    }
    

    // public function orders()
    // {
    //     return $this->hasMany(Ordini::class);
    // }

public function orders()
{
    return $this->hasMany(Ordini::class, 'tag', 'tag');
}

}
