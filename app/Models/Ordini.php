<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Ordini extends Model
{
    use HasFactory;

    protected $table = 'Ordini';

    public function user()
{
    return $this->belongsTo(User::class, 'email', 'email');
}


// Esempio di relazione nel modello Order
public function category()
{
    return $this->belongsTo(ExpenseCategory::class, 'tag', 'tag');
}

}
