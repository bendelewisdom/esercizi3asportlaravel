<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class Categoria extends Model {

    use HasFactory;
    use HasRoles;
    
    protected $table = 'categorie';

    public function articoli() {
        return $this->hasMany(Articoli::class);
    }

    public function taglie() {
        return $this->hasMany(Taglia::class);
    }
}
