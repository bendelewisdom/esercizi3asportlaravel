<?php 
namespace App\Http\Controllers;

use App\Models\ExpenseCategory;
use Illuminate\Http\Request;

class ChartController extends Controller
{
    public function mostraGrafico()
    {
        // Dati di esempio per il grafico
        $labels = ['January', 'February', 'March', 'April', 'May'];
        $data = [65, 59, 80, 81, 56];
    
        return view('/frontend/area-personale/categorie/indexcat', compact('labels', 'data'));
    }

    // public function editcat()
    // {
    //     return view('/frontend/area-personale/categorie/editcat');
    // }

    // public function createcat()
    // {
    //     return view('/frontend/area-personale/categorie/createcat');
    // }

}


