<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ZucchettiController extends Controller
{

    public function getListaTimbrature() {

        // Ottenere la data corrente
        $dataCorrente = date('Y-m-d');
    
        // Ottenere gli orari di timbratura per la data corrente
        $orariTimbratura = [
            $dataCorrente => ["09:00", "13:00", "14:00", "18:00"]
        ];
    
        // Inizializza la somma delle ore totali giornaliere
        $oreTotaliGiornaliere = 0;
    
        // Calcola gli intervalli di tempo tra le timbrature e somma le ore
        for ($i = 0; $i < count($orariTimbratura[$dataCorrente]) - 1; $i++) {
            $timbraturaCorrente = strtotime($orariTimbratura[$dataCorrente][$i]);
            $prossimaTimbratura = strtotime($orariTimbratura[$dataCorrente][$i + 1]);
            $intervallo = $prossimaTimbratura - $timbraturaCorrente;
            $oreTotaliGiornaliere += $intervallo / 3600; // Converti il tempo in ore
        }
    
        // Creare un array con le informazioni richieste
        $risultato = [
            "giorno" => $dataCorrente,
            "orario_timbratura" => $orariTimbratura[$dataCorrente],
            "numero_di_timbrature_fatte_oggi" => count($orariTimbratura[$dataCorrente]),
            "ore_totali_giornaliere" => $oreTotaliGiornaliere,
        ];
    
        return view('frontend/area-personale/listatimbrature')->with('risultato', $risultato);
    }
      

}

