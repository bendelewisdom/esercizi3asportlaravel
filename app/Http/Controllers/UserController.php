<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
public function estrazionedati()
{
    $user= User::all();
    
    return view('/backend/users/index', ['user' => $user]);
    }


    public function visualizzazionepagina($id){
    $user = User::find($id);
    $roles = Role::pluck('name', 'name')->all();
    $userRole = $user->roles->pluck('name', 'name')->all();
        
    return view('/backend/users/edit', compact('user', 'roles', 'userRole'));
    }
                    

    public function update(Request $request, $id){
        $user = User::find($id);
        
        switch($request['action']){
        case 'mod-1': // modifichamo nome, email e ruolo
        $this->validate($request, [
        'name' => 'required',
        'email' => 'required|email|unique:users,email,'.$id,
        'roles' => 'required',
]);
    
        DB::table('model_has_roles')
        ->where('model_id', $id)
        ->delete();
    
        $user->assignRole($request->input('roles'));
        break;
        case 'mod-2': // modifichiamo password
        $this->validate($request, [
        'password' => 'required|confirmed|string|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
]);
        $request['password'] = Hash::make($request['password']);
        break;
        case 'mod-3': // modifichiamo is_active
        $this->validate($request, [
        'is_active'=> 'required'
]);
        break;
}
    
        $input = $request->all();
    
        $user->update($input);
    
        return redirect()->route('user')
        ->with('success', 'User updated successfully.');
    }
    


    public function create(){
        $roles = Role::pluck('name','name')->all();

        return view('/backend/users/create', compact('roles'));
    }

    public function store(Request $request){
        $this->validate($request, [
        'name' => 'required',
        'email' => 'required|email|unique:users,email',
        'password' => 'required|confirmed|string|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
        'roles' => 'required'
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);
        $user->assignRole($request->input('roles'));

        return redirect()->route('user')
        ->with('success', 'User created successfully.');
    }

    
        public function visualizzadminpage()
        {       
        return view('/backend/users/adminpage');
    }
        

        public function visualizzaclientipage()
        {       
        return view('/backend/users/clientipage');
        }


        public function destroy($id){
            // Verifica se l'ID è diverso da null
        if (!is_null($id)) {
                // Trova l'utente con l'ID specificato e eliminalo
        $user = User::find($id);
                
        if ($user) {
        $user->delete();
        return redirect()->route('user')->with('success', 'User deleted successfully.');
        } else {
                    // Se l'utente non è stato trovato, reindirizza con un messaggio di errore
        return redirect()->route('user')->with('error', 'User not found.');
        }
        } else {
                // Se l'ID è null, reindirizza con un messaggio di errore
        return redirect()->route('user')->with('error', 'Invalid ID.');
            }
        }  
        
}
