<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
// use App\Models\NavBrand;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MarchiFornitoriController extends Controller
{
  
        public function elenchimarchifornitori(){
                // $brandModel = new NavBrand();
                // $brands = $brandModel->getAllBrands();
                    $nomiFornitori = [
                        "Mario Rossi",
                        "Luca Bianchi",
                        "Giulia Verdi",
                        "Paolo Neri",
                        "Sara Russo",
                        "Alessandro Ferrari",
                        "Elena Russo",
                        "Giorgio Esposito",
                        "Francesca Romano",
                        "Marco Ricci"
                    ];
           
                    $fornitori = [];
           
                    // Genera 10 nomi casuali selezionando casualmente dall'array
                    for ($i = 0; $i < 10; $i++) {
                        $randomIndex = array_rand($nomiFornitori);
                        $fornitori[] = $nomiFornitori[$randomIndex];
                    }
           
                    // Passa l'array $fornitori alla vista 'fornitori'
            return view('/backend/gestioneFornitori.elenchimarchifornitori')
            // ->with('brands', $brands)
            ->with('fornitori', $fornitori);
        }
    
    
        public function caricamentodocfornitori(){
            return view('/backend/gestioneFornitori.caricamentodocfornitori');
        }
    
}
