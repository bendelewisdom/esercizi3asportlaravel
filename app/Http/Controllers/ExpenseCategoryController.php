<?php

// app/Http/Controllers/ExpenseCategoryController.php

namespace App\Http\Controllers;

use App\Models\ExpenseCategory;
use App\Models\Ordini;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class ExpenseCategoryController extends Controller
{
    // public function index()
    // {
    //     $categories = Auth::user()->expenseCategories;
    //     return view('/frontend/area-personale/categorie', compact('categories'));
    // }

    public function estrazionecat()
    {
        $user = auth()->user(); // Ottieni l'utente autenticato
       
        // Recupera solo le categorie dell'utente autenticato
       
        $cat = ExpenseCategory::where('user_id', $user->id)->get();
        $labels = ['January', 'February', 'March', 'April', 'May'];
        $data = [65, 59, 80, 81, 56];
        return view('/frontend/area-personale/categorie/indexcat')
            ->with('cat', $cat)
            ->with('labels', $labels)
            ->with('data', $data);
    }

    public function createcat()
    {
        return view('/frontend/area-personale/categorie/createcat');
    }

    
    public function categoryfind($id)
    {
        $category = ExpenseCategory::find($id);
        return view('/frontend/area-personale/categorie/editcat', compact('category'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tag' => 'required'
        ]);

        $category = ExpenseCategory::find($id);
        $category->tag = $request->input('tag');
        $category->save();

        return redirect()->route('indexcat')
            ->with('success', 'Category updated successfully.');
    }
    
    // public function destroy($id)
    // {
    //     $categoryd= ExpenseCategory::find($id)->delete();
    //     return redirect()->route('indexcat');
    // }


    
    public function store(Request $request)
    {   
        // Ottieni l'utente autenticato
        $user = auth()->user();
    
        // Esegui la validazione dei dati
        $request->validate([
            'tag' => [
                'required',
                Rule::unique('expense_categories')->where(function ($query) use ($user) {
                    return $query->where('user_id', $user->id);
                }),
            ],
        ]);
    
        // Recupera il valore del campo 'tag' dal form
        $tag = $request->input('tag');
    
        // Assicurati che il campo 'tag' non sia vuoto
        if (empty($tag)) {
            return redirect()->back()->with('error', 'Tag cannot be empty.');
        }
    
        // Crea la nuova categoria associata all'utente autenticato
        $category = ExpenseCategory::create([
            'tag' => $tag,
            'user_id' => $user->id,
        ]);
    
        return redirect()->route('indexcat')->with('success', 'Category created successfully.');
    }
    


    // public function takecat()      
    // {
    //     $user = auth()->user(); // Ottieni l'utente autenticato
       
    //     // Recupera solo le categorie dell'utente autenticato
       
    //     $cat = ExpenseCategory::where('user_id', $user->id)->get();
    //     $labels = ['January', 'February', 'March', 'April', 'May'];
    //     $data = [65, 59, 80, 81, 56];
    //     return view('/frontend/area-personale/categorie/indexcat')
    //         ->with('cat', $cat)
    //         ->with('labels', $labels)
    //         ->with('data', $data);
    // }

    
    public function takecat()
    {
        $user = auth()->user(); // Ottieni l'utente autenticato
    
        // Recupera solo le categorie dell'utente autenticato
        $cat = ExpenseCategory::where('user_id', $user->id)->get(); // Utilizza $cat al posto di $categories
    
        $labels = [];
        $data = [];
        
        // Per ogni categoria, calcola la percentuale di ordini
        foreach ($cat as $category) {
            // Calcola la somma totale delle quantità ordinate per questa categoria di spesa
            $totalQuantityForCategory = Ordini::where('tag', $category->tag)->sum('quantita');
    
            // Calcola la somma totale di tutte le quantità ordinate
            $totalAllQuantity = Ordini::sum('quantita');  
            // Calcola la percentuale di ordini per questa categoria di spesa
            if ($totalAllQuantity > 0) {
                $percentage = ($totalQuantityForCategory / $totalAllQuantity) * 100;
            } else {
                $percentage = 0;
            }
    
            // Aggiungi il tag della categoria e la percentuale al grafico
            $labels[] = $category->tag;
            $data[] = $percentage;
        }
    
        // Passa i dati al frontend per visualizzare il grafico
        return view('/frontend/area-personale/categorie/indexcat')
            ->with('cat', $cat)
            ->with('labels', $labels)
            ->with('data', $data);
    }

public function linhoshop(){
    return view('/linhoshop');
}

}
