<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionsController extends Controller
{
    public function visualizzazionepermissions()
    {       
        $data = Permission::all();
        return view('/backend/permissions/index')
        ->with('data', $data);
    }

        public function visualizzazionepermissionsedit($id)
        {
            $permissions = Permission::find($id);
    
            // return view('/permissions/edit', compact('permission'));
            return view('/backend/permissions/edit', compact('permissions'));
        }

        public function visualizzazionecreate()
        {       
            return view('/backend/permissions/create');
            }


            public function store(Request $request)
            { 
                $this->validate($request, [
                    'name' => 'required|unique:permissions,name',
                ]);
                
                $permission = Permission::create(['name' => $request->input('name')]);
                // dd($request->input('name'));
                
                
                // return redirect()->route('permissions.index')
                //     ->with('success', 'Permission created successfully.');
                return redirect()->route('permissions.index')
                    ->with('success', 'Permission created successfully.');
            }

            // public function show($id)
            // {
            //     $permissions = Permission::find($id);
        
            //     // return view('/permissions/show', compact('permission'));
            //     return view('/backend/permissions/show{id}', compact('permissions'));
            // }


            public function update(Request $request, $id)
            {
                $this->validate($request, [
                    'name' => 'required'
                ]);
        
                $permissions = Permission::find($id);
                $permissions->name = $request->input('name');
                $permissions->save();
        
                return redirect()->route('permissions.index')
                    ->with('success', 'Permission updated successfully.');
            }
        
            /**
             * Remove the specified resource from storage.
             *
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
            public function destroy($id)
            {
                $permissions = Permission::find($id)->delete();
        
                return redirect()->route('permissions.index')
                    ->with('success', 'Permission deleted successfully');
            }
}
