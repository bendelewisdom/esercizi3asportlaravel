<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\Ordini;
use App\Models\Articoli;
use Illuminate\Support\Facades\Http; // Importa la classe Http di Laravel

class OrdiniController extends Controller
{
    public function Ordini($action)
    {
        switch ($action) {
            case 'count':
                return $this->countOrders();
            case 'get':
                return $this->getOrders();
            default:
            return response()->json(['error' => 'Errore durante la richiesta all\'API B']);
        }
    }

    public function countOrders()
    {
        $count = Ordini::count();
        
        return response()->json(['total_orders' => $count]);
    }

    public function getOrders()
    {
        $orders = Ordini::all();
        
        return response()->json(['orders' => $orders]);
    }

    public function prezzoMinore(){ 
       
        try {
            // Ottieni tutti i prodotti ordinati per prezzo crescente e, in caso di prezzi uguali, per ID crescente
            $prodotti = Articoli::orderBy('prezzo', 'asc')->orderBy('id', 'asc')->get();
    
            // Verifica se sono presenti prodotti
            if ($prodotti->isEmpty()) {
                return response()->json(['message' => 'Nessun prodotto trovato'], 404);
            }
    
            return response()->json($prodotti, 200);
    
        } catch (\Exception $e) {
            return response()->json(['message' => 'Errore interno del server'], 500);
        }
        
}


public function getInfoBrandFromOtherProject($brand = null)
{
    $url = 'http://sitoadminltestock.local/api/getInfoBrand';

    if ($brand !== null) {
        $url .= '/' . urlencode($brand);
    }
    try {

        $response = Http::get($url);
        

        if ($response->ok()) {
            $data = $response->json();
            
            return response()->json([
                'success' => true,
                'data' => $data
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Errore durante la richiesta API: ' . $response->status()
            ], 500);
        }
    } catch (\Exception $e) {
        return response()->json([
            'success' => false,
            'message' => 'Errore durante la richiesta API: ' . $e->getMessage()
        ], 500);
    }
}




}