<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http; 

class CallController extends Controller
{

        public function calling(Request $request)
        {
            $urlesterno = 'http://sitoadminltestock.local/api/getInfoBrand';
            $client = new Client();
    
            try {
                $response = $client->request('GET', $urlesterno
            );
    
                $responseData = $response->getBody()->getContents();
                $decodificadati= json_decode($responseData, true);
                // dd($responseData);
                return view('/chiamata', ['data' => $decodificadati
            ]);
            } catch (\Exception $e) {

                return back()->withError('Errore durante la chiamata API: ' . $e->getMessage());
            }
        }

        // public function getJobBoardData(Request $request)
        // {
        //     $urlesterno = 'https://arbeitnow.com/api/job-board-api';
        //     $client = new Client();
    
        //     try {
        //         $response = $client->request('GET', $urlesterno
        //     );
    
        //         $responseData = $response->getBody()->getContents();
        //         $decodificadati= json_decode($responseData, true);
        //         dd($responseData);
        //         return view('/news', ['jobBoardData' => $jobBoardData
        //     ]);
        //     } catch (\Exception $e) {

        //         return back()->withError('Errore durante la chiamata API: ' . $e->getMessage());
        //     }
        // }


        public function getProducts()
        {
            $storeName = 'testshopbenten'; 
            $accessToken = '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'; 
    
            $baseUrl = "https://testshopbenten.myshopify.com/admin/api/2024-04/products.json";
    
            try {
                $response = Http::withHeaders([
                    'X-Shopify-Access-Token' => $accessToken,
                ])->get($baseUrl);
    
                if ($response->successful()) {
                    $products = $response->json()['products'];
                    
                    return response()->json($products);
                } else {
                    return response()->json(['error' => 'Errore durante l\'estrazione dei prodotti Shopify'], 500);
                }
            } catch (Exception $e) {
                return response()->json(['error' => $e->getMessage()], 500);
            }
        }

        public function singleproduct()
        {
            $storeName = 'testshopbenten'; 
            $accessToken = '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';
            $baseUrl = "https://testshopbenten.myshopify.com/admin/api/2024-04/products/9295569289509.json"; 
        
            try {
                $response = Http::withHeaders([
                    'X-Shopify-Access-Token' => $accessToken,
                ])->get($baseUrl);
        
                if ($response->successful()) {
                    $product = $response->json()['product']; 
        
                    return view('/shopify', ['product' => $product]);
                } else {
                    return response()->json(['error' => 'Errore durante l\'estrazione del prodotto Shopify'], 500);
                }
            } catch (Exception $e) {
                return response()->json(['error' => $e->getMessage()], 500);
            }
        }      

}



    
                
