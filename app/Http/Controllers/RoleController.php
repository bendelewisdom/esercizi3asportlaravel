<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class RoleController extends Controller
{
        public function visualizzazionepagina()
        {    
        $datar = DB::table('roles')->get();
        return view('/backend/roles/index')     
        ->with('datar', $datar);
        }
           
        public function store(Request $request)
        {

        $this->validate($request, [
        'name' => 'required|unique:roles,name',
        'permission' => 'required',
        ]);
    
        $role = Role::create(['name' => $request->input('name')]);
        $role->syncPermissions($request->input('permission'));
    
        return redirect()->route('role.index')
        ->with('success', 'Role created successfully.');
        }

        public function visualizzazionedit($id)
        {
        $datar = Role::find($id);
        $permission = Permission::get();
        $rolePermissions = DB::table('role_has_permissions')
        ->where('role_has_permissions.role_id', $id)
        ->pluck('role_has_permissions.permission_id', 'role_has_permissions.permission_id')
        ->all();
    
        return view('/backend/roles/edit', compact('datar', 'permission', 'rolePermissions'));
        }

        public function visualizzazionecreate()
        {       
        $permissions = DB::table('permissions')->get();
        return view('/backend/roles/create')          
        ->with('permissions', $permissions);

        }

        public function update(Request $request, $id)
        {
        $datar = Role::find($id);
        switch ($request['action']) {
        case 'mod-1': // modifichiamo nome e elenco permessi permessi
        $this->validate($request, [
        'name' => 'required',
        'permission' => 'required',
        ]);
    
        $datar->name = $request->input('name');
        $datar->save();
    
        $datar->syncPermissions($request->input('permission'));
        break;
    
        case 'mod-2':
        $this->validate($request, [
        'is_active'=> 'required'
        ]);
        $input = $request->all();
    
        $datar->update($input);
        break;
        }
    
        return redirect()->route('role.index')
        ->with('success', 'Role updated successfully.');
        }


        public function destroy($id)
        {
        $datar = Role::find($id)->delete();

        return redirect()->route('role.index')
        ->with('success', 'Role deleted successfully');
        }
}
