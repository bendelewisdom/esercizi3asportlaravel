<?php

namespace App\Http\Controllers;
use App\Models\Categoria;
use Illuminate\Http\Request;
use App\Models\Articoli;
use App\Models\Taglia;
use App\Models\Colore;
use App\Models\Quantita;
use App\Models\Ordini;
use App\Models\ExpenseCategory;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;



class ArticlesController extends Controller
{
    public function prodotti(){
        $prodotti = Articoli::All();
        return view('/frontend/shop/ecommerce', ['prodotti' => $prodotti]);
    }

    public function type($id) {
        // Seleziona tutte le categorie
        $categorie = Categoria::all();

        $userId = Auth::id();
        // Recupera i nomi delle categorie di spesa solo per l'utente autenticato
        $spese = ExpenseCategory::where('user_id', $userId)->pluck('tag');

        // Trova il prodotto selezionato
        $articoli = Articoli::findOrFail($id);
    
        // Ottieni la categoria_id del prodotto selezionato
        $categoria_id = $articoli->categoria_id;
    
        // Trova le taglie corrispondenti alla categoria_id del prodotto selezionato
        $taglie = Taglia::where('categoria_id', $categoria_id)->pluck('taglia', 'id')->toArray();
    
        // Trova i colori disponibili per il prodotto selezionato
        $colori = Colore::pluck('colore', 'id')->toArray();
    
        // Seleziona tutte le quantità disponibili
        $quantita = Quantita::pluck('quantita', 'id')->toArray();
    
        return view('/frontend/shop/ordinazione', [
            'articoli' => $articoli,
            'spese' => $spese,
            'categorie' => $categorie,
            'taglie' => $taglie,
            'colori' => $colori,
            'quantita' => $quantita
        ]);  
    }

    public function store(Request $request)
    {
        $user = Auth::user();     
        $ordine = new Ordini();
        $ordine->codice_articolo = $request->codice_articolo;
        $ordine->nome = $request->nome;
        $ordine->marca = $request->marca;
        $taglia = Taglia::find($request->taglia)->taglia;    
        $colore = Colore::find($request->colore)->colore;
        $ordine->taglia = $taglia; // Assegna il nome della taglia all'ordine
        $ordine->colore = $colore;  
        // QUELLO GIUSTO ORDINAZIONE
        $res = ExpenseCategory::where("tag", $request->tag)->where("user_id", $user->id)->get()->toArray();       
        $ordine->tag = $res[0]['tag'];

        if ($user) {
            $ordine->email = $user->email;
            $ordine->user_id = $user->id;
        } else {
            // Gestione dell'errore se l'utente non è stato trovato
            // Ad esempio, puoi restituire un errore o eseguire altre azioni di gestione degli errori
            return response()->json(['message' => 'Utente non trovato'], 404);
        }
        $ordine->prezzo = $request->prezzo;
        $ordine->quantita = $request->input('quantita');
        $ordine->created_at = $request->created_at;
        $ordine->save();    
        // Passa i dati dell'ordine alla vista durante il reindirizzamento
        return redirect()->route('listaordini')->with('ordine', $ordine);
    }
    
    public function assignorder()
    {
        $user = auth()->user(); // Ottieni l'utente autenticato
        $ordine = Ordini::where('email', $user->email)->get();        
        return view('/frontend/area-personale/listaordini', ['ordine' => $ordine]);
    } 
}

