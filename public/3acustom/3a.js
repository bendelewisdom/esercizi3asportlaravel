// $( document ).ready(function() {
//     $('#loading').hide();
// });

console.log('custom.js');


$(document).ready(function() {
});

function alertShow() {
    $('#alert').css('display', 'block');
}
function alertHide() {
    $('#alert').css('display','none');
}

function alertDangerShow(msg=null) {
    alertShow();
    if ( msg != null) {
        $('#danger').html(msg);
    }
    $('#danger').css('display', 'flex');

    /*
    setTimeout(function() {
        alertDangerHide();
    }, 5000);
    */

}
function alertSuccessShow(msg=null) {
    alertShow();
    if ( msg != null) {
        $('#success').html(msg);
    }
    $('#success').css('display', 'flex');
    setTimeout(function() {
        alertSuccessHide();
    }, 5000);
}
function alertWarningShow(msg=null) {
    alertShow();
    if ( msg != null) {
        $('#warning').html(msg);
    }
    $('#warning').css('display', 'flex');
}

function alertDangerHide() {
    alertHide();
    $('#danger').css('display', 'none');
}
function alertSuccessHide() {
    $('#success').css('display', 'none');
}
function alertWarningHide() {
    $('#warning').css('display', 'none');
}
$("#col-overlay-close").click(function() {
    overlayColClose();
});
function overlayColClose() {
    $('#col-dx-overlay').removeClass('show');
    $('.cartActionsForm').hide();
}

function overlayColShow() {
    $('#col-dx-overlay').addClass('show');
}

function ordinaTaglie(obj){
    var array = Object.keys(obj).map(function (key) { return obj[key]; });

    //This will sort your array
    function SortByName(a, b){
        var aName = parseFloat(a.size_code);
        var bName = parseFloat(b.size_code);
        return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
    }

    var productSizes = array.sort(SortByName);
    return productSizes;
}
