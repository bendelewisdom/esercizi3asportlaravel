<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Api\OrdiniController;
use App\Http\Controllers\Api\CallController;
use App\Models\Ordini;
use App\Models\Articoli;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/gestioneordini/{action}', [OrdiniController::class, 'Ordini']);


Route::get('/articoli/prezzi', [OrdiniController::class, 'prezzoMinore']);


//TESTAPI
Route::get('/chiamata', [CallController::class, 'calling'])->name('calling');
Route::get('/getProducts', [CallController::class, 'getProducts'])->name('getProducts');
Route::get('/singleproduct', [CallController::class, 'singleproduct'])->name('singleproduct');
// Route::get('/news', [CallController::class, 'getJobBoardData'])->name('getJobBoardData');


Route::get('/getInfoBrandFromOtherProject/{brand?}', [OrdiniController::class, 'getInfoBrandFromOtherProject'])
->name('getInfoBrandFromOtherProject')
->where('brand', '.*');