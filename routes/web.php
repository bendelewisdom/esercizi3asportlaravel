<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PermissionsController;
use App\Http\Controllers\ZucchettiController;
use App\Http\Controllers\ArticlesController;
use App\Http\Controllers\CallController;
use App\Http\Controllers\ExpenseCategoryController;
use App\Http\Controllers\MarchiFornitoriController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Articoli;
use App\Models\Taglia;
use App\Models\Colore;
use App\Models\Categoria;
use App\Models\Quantita;
use App\Models\ExpenseCategory;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//LOGIN
Route::get('/login', function () {
    return view('/vendor/adminlte/auth/login');
})->name('login');

//USERS

Route::get('/backend/users/index', [UserController::class, 'estrazionedati'])->name('user');
Route::get('/backend/users/edit/{id}', [UserController::class, 'visualizzazionepagina'])->name('users.edit');
Route::delete('/backend/users/edit/{id}', [UserController::class, 'destroy'])->name('users.destroy');
Route::get('/backend/users/create', [UserController::class, 'create'])->name('users.create');
Route::post('/backend/users/store', [UserController::class, 'store'])->name('users.store');
Route::patch('/backend/users/update/{id}', [UserController::class, 'update'])->name('users.update');


//RUOLI

Route::get('/backend/roles/index', [RoleController::class, 'visualizzazionepagina'])->name('role.index');
Route::get('/backend/roles/edit/{id}', [RoleController::class, 'visualizzazionedit'])->name('role.edit');
Route::get('/backend/roles/create', [RoleController::class, 'visualizzazionecreate'])->name('role.create');
Route::post('/backend/roles/create', [RoleController::class, 'store'])->name('roles.store');
Route::patch('/backend/roles/update/{id}', [RoleController::class, 'update'])->name('role.update');
Route::delete('/backend/roles/destroy/{id}', [RoleController::class, 'destroy'])->name('role.destroy');


//PERMISSIONS

Route::get('/backend/permissions/index', [PermissionsController::class, 'visualizzazionepermissions'])->name('permissions.index');
Route::get('/backend/permissions/edit/{id}', [PermissionsController::class, 'visualizzazionepermissionsedit'])->name('permissions.edit');
Route::get('/backend/permissions/create', [PermissionsController::class, 'visualizzazionecreate'])->name('permissions.create');
Route::post('/backend/permissions/create', [PermissionsController::class, 'store'])->name('permissions.store');
Route::patch('/backend/permissions/update/{id}', [PermissionsController::class, 'update'])->name('permissions.update');
Route::delete('/backend/permissions/destroy/{id}', [PermissionsController::class, 'destroy'])->name('permissions.destroy');
Route::get('/backend/permissions/adminpage', [UserController::class, 'visualizzadminpage'])->name('adminpage');
Route::get('/backend/permissions/clientipage', [UserController::class, 'visualizzaclientipage'])->name('clientipage');

//TIMBRATURE
Route::get('/frontend/area-personale/listatimbrature', [ZucchettiController::class, 'getListaTimbrature'])->name('lista.timbrature');

//ORDINI
// Route::get('/backend/area-personale/listaordini', [ArticlesController::class, 'visualizzaordini'])->name('lista.ordini');

// //E-COMMERCE
Route::get('/frontend/shop/ecommerce', [ArticlesController::class, 'prodotti'])->name('lista.ecommerce');
Route::get('/frontend/shop/ordinazione/{id}', [ArticlesController::class, 'type'])->name('variants');
Route::post('/frontend/shop/ordinazione', [ArticlesController::class, 'store'])->name('ordinazione');
Route::get('/frontend/area-personale/listaordini', [ArticlesController::class, 'assignorder'])->name('listaordini');

//CHART E CATEGORIE SPESA
Route::get('/frontend/area-personale/categorie/indexcat', [ExpenseCategoryController::class, 'takecat'])->name('indexcat');
Route::get('/frontend/area-personale/categorie/editcat/{id}', [ExpenseCategoryController::class, 'categoryfind'])->name('editcat');
Route::patch('/frontend/area-personale/categorie/editcat/{id}', [ExpenseCategoryController::class, 'update'])->name('catupdate');
Route::get('/frontend/area-personale/categorie/createcat', [ExpenseCategoryController::class, 'createcat'])->name('createcat');
Route::post('/frontend/area-personale/categorie/createcat', [ExpenseCategoryController::class, 'store'])->name('catstore');

//FORNITORI
//Gestione Fornitori
Route::get('/backend/gestioneFornitori.elenchimarchifornitori', [MarchiFornitoriController::class, 'elenchimarchifornitori'])->name('elenchimarchifornitori');
Route::get('/backend/gestioneFornitori.caricamentodocfornitori', [MarchiFornitoriController::class, 'caricamentodocfornitori'])->name('caricamentodocfornitori');


//NEWS
// Route::get('/frontend/area-personale/categorie/indexcat', [ExpenseCategoryController::class, 'takecat'])->name('indexcat');
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
